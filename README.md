# Design Patterns Course Exercises

This repository contains exercises related to design patterns. Each exercise focuses on a specific design pattern and provides a programming task to practice its implementation. The exercises cover creational, structural, and behavioral design patterns.

## Folder Structure

The repository is organized as follows:

- `creational/`: Contains exercises related to creational design patterns.
- `structural/`: Contains exercises related to structural design patterns.
- `behavioral/`: Contains exercises related to behavioral design patterns.

Each folder contains a separate exercise with its own instructions and a starting point for implementation. You are encouraged to complete the exercises by implementing the missing code and exploring different design patterns.

## Getting Started

To get started with the exercises, follow these steps:

1. Clone the repository to your local machine using the following command:
``` bash
git clone https://gitlab.com/ofers2/design-pattern-course.git
```


2. Navigate to the respective folder (`creational/`, `structural/`, or `behavioral/`) containing the exercise you want to work on.

3. Read the exercise instructions provided in the README file within that folder.

4. Implement the missing code based on the exercise requirements. You may choose your preferred programming language.

5. Test your implementation and make any necessary adjustments.

6. Once you have completed an exercise, you can compare your solution with the provided sample solution (if available) in the same folder.

7. Repeat the above steps for other exercises in different design pattern categories.

## Contributing

If you have any improvements or additional exercises to contribute to this repository, feel free to fork the repository and submit a pull request. Contributions are highly appreciated!

## License

This repository is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.
