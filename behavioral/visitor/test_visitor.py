# test_visitor.py

from visitor import Circle, Rectangle, AreaCalculator, ShapePrinter

shapes = [Circle(5), Rectangle(3, 4)]

calculator = AreaCalculator()
printer = ShapePrinter()

for shape in shapes:
    shape.accept(calculator)
    shape.accept(printer)
