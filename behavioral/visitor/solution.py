# solution.py

from abc import ABC, abstractmethod


class Shape(ABC):
    @abstractmethod
    def accept(self, visitor):
        pass


class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def accept(self, visitor):
        visitor.visit_circle(self)


class Rectangle(Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def accept(self, visitor):
        visitor.visit_rectangle(self)


class AreaCalculator:
    def visit_circle(self, circle):
        area = 3.14 * circle.radius ** 2
        print(f"Circle with radius {circle.radius} has area: {area}")

    def visit_rectangle(self, rectangle):
        area = rectangle.width * rectangle.height
        print(f"Rectangle with width {rectangle.width} and height {rectangle.height} has area: {area}")


class ShapePrinter:
    def visit_circle(self, circle):
        print(f"Circle with radius {circle.radius}")

    def visit_rectangle(self, rectangle):
        print(f"Rectangle with width {rectangle.width} and height {rectangle.height}")


# Example usage
if __name__ == "__main__":
    shapes = [Circle(5), Rectangle(3, 4)]

    calculator = AreaCalculator()
    printer = ShapePrinter()

    for shape in shapes:
        shape.accept(calculator)
        shape.accept(printer)
