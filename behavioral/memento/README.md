# Exercise: Memento Design Pattern

In this exercise, you will practice implementing the Memento design pattern.

## Instructions

1. Open the `memento.py` file.

2. Identify a scenario where you need to capture and restore an object's state, such as undo/redo functionality or saving and restoring application settings.

3. Implement the Memento pattern by creating three main classes: `Originator`, `Memento`, and `Caretaker`.

4. The `Originator` class represents the object whose state needs to be saved and restored. It should provide methods to set and get its state, as well as create and restore mementos.

5. The `Memento` class represents the saved state of the `Originator`. It should store the state and provide methods to retrieve the saved state.

6. The `Caretaker` class is responsible for managing the mementos. It should have methods to save a memento, retrieve a memento, and restore the state of the `Originator` from a memento.

7. Demonstrate the functionality of the Memento pattern by using the `Originator`, `Memento`, and `Caretaker` classes to save and restore the state of the `Originator` object.

## Getting Started

The starting point for this exercise is an empty `memento.py` file. You need to implement the Memento pattern and create the necessary classes and methods from scratch.

## Example Scenario

Suppose you are building a text editor application that needs undo/redo functionality. You want to implement the Memento pattern to save and restore the state of the text editor.

In this scenario, you can use the Memento pattern to implement an `Editor` class as the `Originator` that represents the text editor. The `Editor` should have methods to set and get the text, as well as create and restore mementos of the text state. The `Memento` class can store the saved text state, and the `Caretaker` class can manage the mementos and provide methods to save, retrieve, and restore the text state.

## Testing

To test your implementation, create an `Editor` object and perform various operations on it, such as setting the text, saving the state, making changes, saving the new state, and then restoring the previous state. Verify that the text is properly saved and restored using the mementos.

## Conclusion

The Memento design pattern allows objects to capture and restore their internal state without violating encapsulation. It is useful in scenarios where you need to implement undo/redo functionality, save and restore application settings, or manage the history of an object's state.

Completing this exercise will help you understand the Memento pattern and its application in maintaining and restoring object state.

Good luck with the exercise!
