# test_memento.py

from memento import Originator, Caretaker

originator = Originator()
caretaker = Caretaker()

# Set initial state
originator.set_state("State 1")
caretaker.add_memento(originator.create_memento())

# Modify state
originator.set_state("State 2")
caretaker.add_memento(originator.create_memento())

# Restore to previous state
memento = caretaker.get_memento(0)
originator.restore_from_memento(memento)

# Check state
print(originator.get_state())  # Output: State 1
