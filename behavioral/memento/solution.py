# solution.py

class Memento:
    def __init__(self, state):
        self._state = state

    def get_state(self):
        return self._state


class Originator:
    def __init__(self):
        self._state = ""

    def set_state(self, state):
        self._state = state

    def create_memento(self):
        return Memento(self._state)

    def restore_from_memento(self, memento):
        self._state = memento.get_state()

    def get_state(self):
        return self._state


class Caretaker:
    def __init__(self):
        self._mementos = []

    def add_memento(self, memento):
        self._mementos.append(memento)

    def get_memento(self, index):
        return self._mementos[index]


# Example usage
if __name__ == "__main__":
    originator = Originator()
    caretaker = Caretaker()

    # Set initial state
    originator.set_state("State 1")
    caretaker.add_memento(originator.create_memento())

    # Modify state
    originator.set_state("State 2")
    caretaker.add_memento(originator.create_memento())

    # Restore to previous state
    memento = caretaker.get_memento(0)
    originator.restore_from_memento(memento)

    # Check state
    print(originator.get_state())  # Output: State 1
