# Exercise: Strategy Design Pattern

In this exercise, you will practice implementing the Strategy design pattern.

## Instructions

1. Open the `strategy.py` file.

2. Identify a scenario where multiple algorithms can be applied interchangeably to perform a specific task.

3. Implement the Strategy pattern by creating the necessary classes and methods.

4. Define a `Context` class that maintains a reference to the current strategy object.

5. Create concrete strategy classes that encapsulate different algorithms.

6. Implement methods in the `Context` class to delegate the task to the current strategy object.

7. Demonstrate the functionality of the Strategy pattern by creating an instance of the `Context` class and applying different strategies.

## Getting Started

The starting point for this exercise is an empty `strategy.py` file. You need to implement the Strategy pattern and create the necessary classes and methods from scratch.

## Example Scenario

Suppose you are developing a sorting algorithm library. The library supports different sorting algorithms such as bubble sort, insertion sort, and merge sort. Each algorithm has a different implementation, but they all fulfill the same task of sorting a list of elements.

In this scenario, you can use the Strategy pattern by implementing a `SortContext` class as the `Context` that represents the sorting context. The different sorting algorithms can be implemented as concrete strategy classes, such as `BubbleSortStrategy`, `InsertionSortStrategy`, and `MergeSortStrategy`, that define the specific sorting algorithm.

## Testing

To test your implementation, create an instance of the `Context` class and apply different strategies using the appropriate methods. Verify that the task is performed correctly according to the selected strategy.

## Conclusion

The Strategy design pattern allows an object to select an algorithm at runtime and switch it interchangeably. It promotes flexibility, extensibility, and maintainability by encapsulating each algorithm in a separate strategy class.

Completing this exercise will help you understand the Strategy pattern and its application in scenarios where different algorithms can be used interchangeably to perform a specific task.

Good luck with the exercise!
