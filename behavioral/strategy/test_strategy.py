# test_strategy.py

from strategy import Context, ConcreteStrategyA, ConcreteStrategyB

context = Context(ConcreteStrategyA())
context.perform_task()

context.set_strategy(ConcreteStrategyB())
context.perform_task()
