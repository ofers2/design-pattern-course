# solution.py

class Context:
    def __init__(self, strategy):
        self._strategy = strategy

    def set_strategy(self, strategy):
        self._strategy = strategy

    def perform_task(self):
        self._strategy.execute()


class Strategy:
    def execute(self):
        pass


class ConcreteStrategyA(Strategy):
    def execute(self):
        print("Executing strategy A")


class ConcreteStrategyB(Strategy):
    def execute(self):
        print("Executing strategy B")


# Example usage
if __name__ == "__main__":
    context = Context(ConcreteStrategyA())
    context.perform_task()

    context.set_strategy(ConcreteStrategyB())
    context.perform_task()
