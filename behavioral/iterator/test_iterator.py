# test_iterator.py

from iterator import Playlist, PlaylistIterator

# Create a playlist
playlist = Playlist()
playlist.add_song("Song 1")
playlist.add_song("Song 2")
playlist.add_song("Song 3")
playlist.add_song("Song 4")
playlist.add_song("Song 5")

# Get an iterator for the playlist
iterator = playlist.create_iterator()

# Iterate over the playlist and print the songs
while iterator.has_next():
    song = iterator.next()
    print(f"Playing: {song}")
