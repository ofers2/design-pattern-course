# Exercise: Iterator Design Pattern

In this exercise, you will practice implementing the Iterator design pattern.

## Instructions

1. Open the `iterator.py` file.

2. Identify a collection or aggregate object that you want to iterate over. This could be a list, array, tree, graph, or any other data structure.

3. Implement an iterator class that provides an interface for iterating over the elements of the collection. The iterator should maintain the current position within the collection and provide methods to move to the next element, check if there are more elements, and retrieve the current element.

4. Integrate the iterator class into the collection or aggregate object, providing a method to create and return an iterator object.

5. Use the iterator to iterate over the elements of the collection, perform any desired operations on each element, and demonstrate the functionality of the Iterator pattern.

## Getting Started

The starting point for this exercise is an empty `iterator.py` file. You need to implement the Iterator pattern and create the necessary classes and methods from scratch.

## Example Scenario

Suppose you are building a music player application that maintains a playlist of songs. You want to provide an iterator to iterate over the songs in the playlist and perform operations such as playing, skipping, or shuffling the songs.

In this scenario, you can use the Iterator pattern to implement an iterator class for the playlist collection. The iterator will allow you to traverse the songs one by one, retrieve the current song, and move to the next song in the playlist.

By using the Iterator pattern, you can decouple the iteration logic from the collection and provide a consistent and uniform way to access the elements of the collection.

## Testing

To test your implementation, create a collection object (e.g., a playlist) and populate it with some elements (e.g., songs). Retrieve an iterator from the collection and use it to iterate over the elements. Perform any desired operations on each element and verify that the iteration works as expected.

## Conclusion

The Iterator design pattern provides a way to access the elements of a collection or aggregate object sequentially without exposing the underlying structure. It encapsulates the iteration logic within an iterator object, allowing clients to traverse the collection in a uniform manner.

Completing this exercise will help you understand the Iterator pattern and its application in scenarios where you need to iterate over and process the elements of a collection.

Good luck with the exercise!
