# solution.py

from abc import ABC, abstractmethod


# Abstract base class for the iterator
class Iterator(ABC):
    @abstractmethod
    def has_next(self):
        pass

    @abstractmethod
    def next(self):
        pass


# Concrete iterator class for the collection
class PlaylistIterator(Iterator):
    def __init__(self, playlist):
        self.playlist = playlist
        self.current_index = 0

    def has_next(self):
        return self.current_index < len(self.playlist)

    def next(self):
        if self.has_next():
            song = self.playlist[self.current_index]
            self.current_index += 1
            return song
        else:
            raise StopIteration()


# Concrete collection class
class Playlist:
    def __init__(self):
        self.songs = []

    def add_song(self, song):
        self.songs.append(song)

    def create_iterator(self):
        return PlaylistIterator(self.songs)


# Example usage
if __name__ == "__main__":
    # Create a playlist
    playlist = Playlist()
    playlist.add_song("Song 1")
    playlist.add_song("Song 2")
    playlist.add_song("Song 3")
    playlist.add_song("Song 4")
    playlist.add_song("Song 5")

    # Get an iterator for the playlist
    iterator = playlist.create_iterator()

    # Iterate over the playlist and print the songs
    while iterator.has_next():
        song = iterator.next()
        print(f"Playing: {song}")
