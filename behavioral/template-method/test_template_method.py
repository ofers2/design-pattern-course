# test_template_method.py

from template_method import Warrior, Wizard

warrior = Warrior()
warrior.perform_turn()

wizard = Wizard()
wizard.perform_turn()
