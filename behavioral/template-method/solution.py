# solution.py

from abc import ABC, abstractmethod


class Character(ABC):
    def perform_turn(self):
        self.start_turn()
        self.perform_action()
        self.end_turn()

    def start_turn(self):
        print("Starting turn")

    @abstractmethod
    def perform_action(self):
        pass

    def end_turn(self):
        print("Ending turn")


class Warrior(Character):
    def perform_action(self):
        print("Performing attack")


class Wizard(Character):
    def perform_action(self):
        print("Casting spell")


# Example usage
if __name__ == "__main__":
    warrior = Warrior()
    warrior.perform_turn()

    wizard = Wizard()
    wizard.perform_turn()
