# Exercise: Template Method Design Pattern

In this exercise, you will practice implementing the Template Method design pattern.

## Instructions

1. Open the `template_method.py` file.

2. Identify a scenario where a process consists of multiple steps with a fixed sequence but varying implementations.

3. Implement the Template Method pattern by creating the necessary classes and methods.

4. Define an abstract class that represents the template with a series of steps as methods.

5. Encapsulate the common steps in the template class and leave the specific steps to be implemented by subclasses.

6. Implement concrete subclasses that provide their own implementations for the specific steps.

7. Demonstrate the functionality of the Template Method pattern by creating instances of the concrete subclasses and invoking the template method.

## Getting Started

The starting point for this exercise is an empty `template_method.py` file. You need to implement the Template Method pattern and create the necessary classes and methods from scratch.

## Example Scenario

Suppose you are developing a game that involves different characters with varying abilities. Each character has a unique set of skills, but they all follow the same sequence of actions during a turn: start turn, perform action, end turn.

In this scenario, you can use the Template Method pattern by creating an abstract `Character` class as the template. The `Character` class will define the template method `perform_turn` that encapsulates the common sequence of actions. Concrete character classes, such as `Warrior` and `Wizard`, will extend the `Character` class and implement the specific actions for their abilities.

## Testing

To test your implementation, create instances of the concrete character classes and invoke the `perform_turn` method. Verify that the sequence of actions is executed correctly according to the specific character's implementation.

## Conclusion

The Template Method design pattern provides a way to define the outline of an algorithm in a base class while allowing subclasses to provide their own implementations for certain steps. It promotes code reusability, flexibility, and encapsulation of the common behavior in the template class.

Completing this exercise will help you understand the Template Method pattern and its application in scenarios where a process consists of multiple steps with a fixed sequence but varying implementations.

Good luck with the exercise!
