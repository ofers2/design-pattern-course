# test_state.py

from state import Context, PlayState, PauseState, StopState

context = Context(PlayState())
context.perform_operation()

context.set_state(PauseState())
context.perform_operation()

context.set_state(StopState())
context.perform_operation()
