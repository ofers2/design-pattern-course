# solution.py

class Context:
    def __init__(self, state):
        self._state = state

    def set_state(self, state):
        self._state = state

    def perform_operation(self):
        self._state.perform_operation(self)


class State:
    def perform_operation(self, context):
        pass


class PlayState(State):
    def perform_operation(self, context):
        print("Playing music")


class PauseState(State):
    def perform_operation(self, context):
        print("Pausing music")


class StopState(State):
    def perform_operation(self, context):
        print("Stopping music")


# Example usage
if __name__ == "__main__":
    context = Context(PlayState())
    context.perform_operation()

    context.set_state(PauseState())
    context.perform_operation()

    context.set_state(StopState())
    context.perform_operation()
