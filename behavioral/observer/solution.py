# solution.py

class Subject:
    def __init__(self):
        self._observers = []

    def register_observer(self, observer):
        self._observers.append(observer)

    def unregister_observer(self, observer):
        self._observers.remove(observer)

    def notify_observers(self, data):
        for observer in self._observers:
            observer.update(data)


class Observer:
    def update(self, data):
        pass


# Example usage
if __name__ == "__main__":
    class DisplayComponent(Observer):
        def update(self, data):
            print("Received data:", data)

    subject = Subject()
    observer1 = DisplayComponent()
    observer2 = DisplayComponent()

    subject.register_observer(observer1)
    subject.register_observer(observer2)

    subject.notify_observers("Weather data: 25°C, sunny")
