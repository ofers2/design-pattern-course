# Exercise: Observer Design Pattern

In this exercise, you will practice implementing the Observer design pattern.

## Instructions

1. Open the `observer.py` file.

2. Identify a scenario where there is a one-to-many relationship between objects, and changes in one object need to be propagated to multiple dependent objects.

3. Implement the Observer pattern by creating the `Subject` and `Observer` classes.

4. The `Subject` class should have methods to register observers, unregister observers, and notify all registered observers when its state changes.

5. The `Observer` class should define an update method that is called by the `Subject` to notify it of changes.

6. Demonstrate the functionality of the Observer pattern by creating instances of the `Subject` and multiple `Observer` objects. Register the observers with the subject and observe the changes in the subject's state.

## Getting Started

The starting point for this exercise is an empty `observer.py` file. You need to implement the Observer pattern and create the necessary classes and methods from scratch.

## Example Scenario

Suppose you are building a weather monitoring application. You have a `WeatherStation` class that represents the source of weather data. You want to implement the Observer pattern to notify various display components whenever the weather data changes.

In this scenario, you can use the Observer pattern by creating a `WeatherData` class as the `Subject` that represents the weather data. The `WeatherData` class should provide methods to register and unregister observers, as well as notify all registered observers when the weather data changes. The display components can be implemented as `Observer` classes that define an update method to receive the updated weather data.

## Testing

To test your implementation, create instances of the `Subject` and multiple `Observer` objects. Register the observers with the subject and perform operations that change the subject's state. Verify that the registered observers are notified and receive the updated data.

## Conclusion

The Observer design pattern enables loose coupling between objects, allowing changes in one object to be communicated to multiple dependent objects. It is useful in scenarios where there is a one-to-many relationship between objects and the state changes in one object need to be propagated to other objects.

Completing this exercise will help you understand the Observer pattern and its application in building reactive and event-driven systems.

Good luck with the exercise!
