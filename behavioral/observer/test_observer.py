# test_observer.py

from observer import Subject, Observer

class DisplayComponent(Observer):
    def update(self, data):
        print("Received data:", data)

subject = Subject()
observer1 = DisplayComponent()
observer2 = DisplayComponent()

subject.register_observer(observer1)
subject.register_observer(observer2)

subject.notify_observers("Weather data: 25°C, sunny")
