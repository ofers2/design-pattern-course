# test_mediator.py

from mediator import ChatMediator, User

# Create a chat mediator
chat_mediator = ChatMediator()

# Create users and register them with the mediator
user1 = User("Alice", chat_mediator)
user2 = User("Bob", chat_mediator)
user3 = User("Charlie", chat_mediator)
chat_mediator.register_colleague(user1)
chat_mediator.register_colleague(user2)
chat_mediator.register_colleague(user3)

# Simulate message exchange
user1.send_message("Hello, everyone!")
user2.send_message("Hi, Alice!")
user3.send_message("Hey there!")
