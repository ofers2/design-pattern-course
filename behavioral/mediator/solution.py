# solution.py

from abc import ABC, abstractmethod


# Abstract Mediator
class Mediator(ABC):
    @abstractmethod
    def send_message(self, message: str, sender: 'Colleague') -> None:
        pass


# Concrete Mediator
class ChatMediator(Mediator):
    def __init__(self):
        self.colleagues = []

    def register_colleague(self, colleague: 'Colleague') -> None:
        self.colleagues.append(colleague)

    def send_message(self, message: str, sender: 'Colleague') -> None:
        for colleague in self.colleagues:
            if colleague != sender:
                colleague.receive_message(message)


# Abstract Colleague
class Colleague(ABC):
    def __init__(self, mediator: Mediator):
        self.mediator = mediator

    @abstractmethod
    def send_message(self, message: str) -> None:
        pass

    @abstractmethod
    def receive_message(self, message: str) -> None:
        pass


# Concrete Colleague
class User(Colleague):
    def __init__(self, name: str, mediator: Mediator):
        super().__init__(mediator)
        self.name = name

    def send_message(self, message: str) -> None:
        self.mediator.send_message(message, self)

    def receive_message(self, message: str) -> None:
        print(f"{self.name} received message: {message}")


# Example usage
if __name__ == "__main__":
    # Create a chat mediator
    chat_mediator = ChatMediator()

    # Create users and register them with the mediator
    user1 = User("Alice", chat_mediator)
    user2 = User("Bob", chat_mediator)
    user3 = User("Charlie", chat_mediator)
    chat_mediator.register_colleague(user1)
    chat_mediator.register_colleague(user2)
    chat_mediator.register_colleague(user3)

    # Simulate message exchange
    user1.send_message("Hello, everyone!")
    user2.send_message("Hi, Alice!")
    user3.send_message("Hey there!")

