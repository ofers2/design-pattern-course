# Exercise: Mediator Design Pattern

In this exercise, you will practice implementing the Mediator design pattern.

## Instructions

1. Open the `mediator.py` file.

2. Identify a system or scenario where multiple objects need to communicate with each other, but you want to reduce the direct dependencies between them.

3. Implement a mediator class that encapsulates the communication logic between the objects. The mediator should provide methods for objects to register themselves, send messages, and handle the communication between objects.

4. Integrate the mediator into the participating objects, allowing them to send messages and receive messages through the mediator.

5. Demonstrate the functionality of the Mediator pattern by simulating interactions between the participating objects through the mediator.

## Getting Started

The starting point for this exercise is an empty `mediator.py` file. You need to implement the Mediator pattern and create the necessary classes and methods from scratch.

## Example Scenario

Suppose you are building a chat application where users can send messages to each other. You want to implement a mediator that facilitates communication between the users. Instead of each user directly sending messages to other users, they will send messages through the mediator, which will handle the routing and delivery of messages.

In this scenario, you can use the Mediator pattern to implement a `ChatMediator` class that maintains a list of registered users and provides methods for users to send messages and receive messages. The users will communicate with each other indirectly through the mediator, reducing the direct dependencies between them.

## Testing

To test your implementation, create a few user objects and register them with the mediator. Simulate message exchanges between the users by having them send messages through the mediator. Verify that the messages are properly routed and delivered to the intended recipients.

## Conclusion

The Mediator design pattern promotes loose coupling between objects by encapsulating the communication logic within a mediator object. It allows objects to communicate with each other indirectly through the mediator, reducing the direct dependencies between them.

Completing this exercise will help you understand the Mediator pattern and its application in scenarios where you need to facilitate communication and coordination between multiple objects.

Good luck with the exercise!
