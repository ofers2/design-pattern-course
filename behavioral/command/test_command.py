# test_command.py

from command import (
    TurnOnCommand,
    TurnOffCommand,
    ChangeChannelCommand,
    VolumeUpCommand,
    VolumeDownCommand,
    RemoteControl,
    TV,
)

# Create the objects
tv = TV()
remote_control = RemoteControl()

# Create the commands
turn_on_command = TurnOnCommand(tv)
turn_off_command = TurnOffCommand(tv)
change_channel_command = ChangeChannelCommand(tv, 7)
volume_up_command = VolumeUpCommand(tv)
volume_down_command = VolumeDownCommand(tv)

# Associate the commands with the invoker
remote_control.set_command(turn_on_command)
remote_control.set_command(change_channel_command)
remote_control.set_command(volume_up_command)

# Execute the commands
remote_control.execute_commands()
