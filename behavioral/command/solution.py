# solution.py

from abc import ABC, abstractmethod


# Abstract base class for commands
class Command(ABC):
    @abstractmethod
    def execute(self):
        pass


# Concrete command classes
class TurnOnCommand(Command):
    def __init__(self, tv):
        self.tv = tv

    def execute(self):
        self.tv.turn_on()


class TurnOffCommand(Command):
    def __init__(self, tv):
        self.tv = tv

    def execute(self):
        self.tv.turn_off()


class ChangeChannelCommand(Command):
    def __init__(self, tv, channel):
        self.tv = tv
        self.channel = channel

    def execute(self):
        self.tv.change_channel(self.channel)


class VolumeUpCommand(Command):
    def __init__(self, tv):
        self.tv = tv

    def execute(self):
        self.tv.volume_up()


class VolumeDownCommand(Command):
    def __init__(self, tv):
        self.tv = tv

    def execute(self):
        self.tv.volume_down()


# Invoker class
class RemoteControl:
    def __init__(self):
        self.commands = []

    def set_command(self, command):
        self.commands.append(command)

    def execute_commands(self):
        for command in self.commands:
            command.execute()


# Receiver class
class TV:
    def turn_on(self):
        print("TV turned on.")

    def turn_off(self):
        print("TV turned off.")

    def change_channel(self, channel):
        print(f"TV channel changed to {channel}.")

    def volume_up(self):
        print("TV volume increased.")

    def volume_down(self):
        print("TV volume decreased.")


# Create the objects
tv = TV()
remote_control = RemoteControl()

# Create the commands
turn_on_command = TurnOnCommand(tv)
turn_off_command = TurnOffCommand(tv)
change_channel_command = ChangeChannelCommand(tv, 7)
volume_up_command = VolumeUpCommand(tv)
volume_down_command = VolumeDownCommand(tv)

# Associate the commands with the invoker
remote_control.set_command(turn_on_command)
remote_control.set_command(change_channel_command)
remote_control.set_command(volume_up_command)

# Execute the commands
remote_control.execute_commands()
