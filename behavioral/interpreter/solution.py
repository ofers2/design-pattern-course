# solution.py

from abc import ABC, abstractmethod


# Abstract base class for expressions
class Expression(ABC):
    @abstractmethod
    def interpret(self, context):
        pass


# Concrete expression classes
class NumberExpression(Expression):
    def __init__(self, value):
        self.value = value

    def interpret(self, context):
        return self.value


class AdditionExpression(Expression):
    def __init__(self, left_expression, right_expression):
        self.left_expression = left_expression
        self.right_expression = right_expression

    def interpret(self, context):
        return (
            self.left_expression.interpret(context)
            + self.right_expression.interpret(context)
        )


class SubtractionExpression(Expression):
    def __init__(self, left_expression, right_expression):
        self.left_expression = left_expression
        self.right_expression = right_expression

    def interpret(self, context):
        return (
            self.left_expression.interpret(context)
            - self.right_expression.interpret(context)
        )


# Context class
class Context:
    def __init__(self):
        self.variables = {}


# Client code
def evaluate(expression, context):
    return expression.interpret(context)


# Example usage
if __name__ == "__main__":
    context = Context()

    # Define the expressions
    number1 = NumberExpression(10)
    number2 = NumberExpression(5)
    addition = AdditionExpression(number1, number2)
    subtraction = SubtractionExpression(number1, number2)

    # Evaluate the expressions
    result1 = evaluate(addition, context)
    result2 = evaluate(subtraction, context)

    # Print the results
    print(f"Result of addition: {result1}")
    print(f"Result of subtraction: {result2}")
