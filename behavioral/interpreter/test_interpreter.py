# test_interpreter.py

from interpreter import (
    NumberExpression,
    AdditionExpression,
    SubtractionExpression,
    evaluate,
    Context,
)

# Create a context
context = Context()

# Define the expressions
number1 = NumberExpression(10)
number2 = NumberExpression(5)
addition = AdditionExpression(number1, number2)
subtraction = SubtractionExpression(number1, number2)

# Evaluate the expressions
result1 = evaluate(addition, context)
result2 = evaluate(subtraction, context)

# Print the results
print(f"Result of addition: {result1}")
print(f"Result of subtraction: {result2}")
