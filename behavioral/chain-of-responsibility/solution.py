# solution.py

from abc import ABC, abstractmethod


# Abstract base class for handlers
class Handler(ABC):
    def __init__(self, successor=None):
        self.successor = successor

    @abstractmethod
    def handle_request(self, request):
        pass


# Concrete handler classes
class ITSupportHandler(Handler):
    def handle_request(self, request):
        if request == "IT Support":
            print("IT Support Handler: Handling the request.")
        elif self.successor:
            self.successor.handle_request(request)


class CustomerServiceHandler(Handler):
    def handle_request(self, request):
        if request == "Customer Service":
            print("Customer Service Handler: Handling the request.")
        elif self.successor:
            self.successor.handle_request(request)


class ManagementHandler(Handler):
    def handle_request(self, request):
        if request == "Management":
            print("Management Handler: Handling the request.")
        elif self.successor:
            self.successor.handle_request(request)


# Create the chain of responsibility
it_support_handler = ITSupportHandler()
customer_service_handler = CustomerServiceHandler()
management_handler = ManagementHandler()

it_support_handler.successor = customer_service_handler
customer_service_handler.successor = management_handler

# Test the chain
it_support_handler.handle_request("IT Support")
it_support_handler.handle_request("Customer Service")
it_support_handler.handle_request("Management")
it_support_handler.handle_request("Finance")
