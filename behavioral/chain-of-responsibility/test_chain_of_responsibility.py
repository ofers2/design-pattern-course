# test_chain_of_responsibility.py

from chain_of_responsibility import (
    ITSupportHandler,
    CustomerServiceHandler,
    ManagementHandler,
)

# Create the chain of responsibility
it_support_handler = ITSupportHandler()
customer_service_handler = CustomerServiceHandler()
management_handler = ManagementHandler()

it_support_handler.successor = customer_service_handler
customer_service_handler.successor = management_handler

# Test the chain
it_support_handler.handle_request("IT Support")
it_support_handler.handle_request("Customer Service")
it_support_handler.handle_request("Management")
it_support_handler.handle_request("Finance")
