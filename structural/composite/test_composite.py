# test_composite.py

from composite import Leaf, Composite

# Create individual Leaf objects
leaf1 = Leaf()
leaf2 = Leaf()
leaf3 = Leaf()

# Create a Composite object
composite = Composite()

# Add Leaf objects to the Composite
composite.add(leaf1)
composite.add(leaf2)

# Call the operation method on the Composite
composite.operation()

# Remove a Leaf from the Composite
composite.remove(leaf2)

# Call the operation method on the Composite again
composite.operation()

# Create a new Composite object
composite2 = Composite()

# Add the original Composite and a Leaf to the new Composite
composite2.add(composite)
composite2.add(leaf3)

# Call the operation method on the new Composite
composite2.operation()
