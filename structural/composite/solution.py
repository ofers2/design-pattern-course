# solution.py

class Component:
    def operation(self):
        pass


class Leaf(Component):
    def operation(self):
        print("Leaf operation called")


class Composite(Component):
    def __init__(self):
        self.children = []

    def add(self, component):
        self.children.append(component)

    def remove(self, component):
        self.children.remove(component)

    def operation(self):
        print("Composite operation called")
        for child in self.children:
            child.operation()
