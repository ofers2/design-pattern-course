# Exercise: Proxy Design Pattern

In this exercise, you will practice implementing the Proxy design pattern.

## Instructions

1. Open the `proxy.py` file.

2. Implement the Proxy pattern to provide a surrogate or placeholder for another object.

3. Identify a class or set of classes that you want to add an extra layer of indirection or control access to.

4. Create a `Subject` interface or base class that both the real object and proxy implement. This ensures that the proxy can be used wherever the real object is expected.

5. Implement a `RealSubject` class that represents the real object and performs the actual operations.

6. Create a `Proxy` class that acts as a surrogate for the real object. The proxy should implement the same interface as the real object and delegate the calls to the real object.

7. Add any additional functionality or behavior to the proxy as needed, such as access control or caching.

8. Test your implementation by creating an instance of the proxy and invoking its methods. Verify that the proxy properly delegates the calls to the real object and performs any additional functionality.

## Getting Started

The starting point for this exercise is an empty `proxy.py` file. You need to implement the Proxy pattern and create the necessary classes from scratch.

## Example Scenario

Suppose you are developing a document management system that loads and displays large documents from a remote server. Loading and rendering the entire document can be time-consuming and resource-intensive.

In this scenario, you can use the Proxy pattern to create a `DocumentProxy` that acts as a placeholder for the actual `Document` object. The proxy can lazily load and render only the necessary portions of the document when requested, providing faster access and conserving resources.

By using the Proxy pattern, you can add an extra layer of indirection and control over the document loading and rendering process.

## Testing

To test your implementation, create an instance of the `DocumentProxy` and invoke its methods. Verify that the proxy properly delegates the calls to the real `Document` object and performs any additional functionality you have implemented.

## Conclusion

The Proxy design pattern provides a surrogate or placeholder for another object, allowing for additional control and indirection. It can be useful in scenarios where you want to control access to an object, add extra functionality, or optimize resource usage.

By successfully completing this exercise, you will gain a better understanding of the Proxy pattern and its application in real-world scenarios.

Good luck with the exercise!
