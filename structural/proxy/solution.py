# solution.py

from abc import ABC, abstractmethod


# Interface or base class for the subject
class Subject(ABC):
    @abstractmethod
    def perform_operation(self):
        pass


# RealSubject class that represents the real object
class RealSubject(Subject):
    def perform_operation(self):
        print("RealSubject: Performing operation...")


# Proxy class that acts as a surrogate for the real object
class Proxy(Subject):
    def __init__(self, real_subject):
        self.real_subject = real_subject

    def perform_operation(self):
        print("Proxy: Before operation...")
        self.real_subject.perform_operation()
        print("Proxy: After operation...")


# Create an instance of the real object
real_subject = RealSubject()

# Create an instance of the proxy and pass the real object as an argument
proxy = Proxy(real_subject)

# Call the method on the proxy, which will delegate the call to the real object
proxy.perform_operation()
