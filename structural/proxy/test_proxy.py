# test_proxy.py

from proxy import RealSubject, Proxy

# Create an instance of the real object
real_subject = RealSubject()

# Create an instance of the proxy and pass the real object as an argument
proxy = Proxy(real_subject)

# Call the method on the proxy, which will delegate the call to the real object
proxy.perform_operation()
