# test_bridge.py

from bridge import Abstraction, RefinedAbstraction, ConcreteImplementorA, ConcreteImplementorB

# Create an instance of the Abstraction with ConcreteImplementorA
abstraction = Abstraction(ConcreteImplementorA())

# Call the operation method on the Abstraction
abstraction.operation()

# Create an instance of the RefinedAbstraction with ConcreteImplementorB
refined_abstraction = RefinedAbstraction(ConcreteImplementorB())

# Call the operation method on the RefinedAbstraction
refined_abstraction.operation()
refined_abstraction.additional_operation()
