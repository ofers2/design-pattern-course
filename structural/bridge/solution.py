# solution.py

class Abstraction:
    def __init__(self, implementor):
        self.implementor = implementor

    def operation(self):
        self.implementor.do_something()


class RefinedAbstraction(Abstraction):
    def additional_operation(self):
        pass


class Implementor:
    def do_something(self):
        pass


class ConcreteImplementorA(Implementor):
    def do_something(self):
        print("ConcreteImplementorA is doing something...")


class ConcreteImplementorB(Implementor):
    def do_something(self):
        print("ConcreteImplementorB is doing something...")
