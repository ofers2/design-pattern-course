# test_adapter.py

from adapter import Target, Adaptee, Adapter

# Create an instance of the Adaptee
adaptee = Adaptee()

# Create an Adapter that adapts the Adaptee to the Target interface
adapter = Adapter(adaptee)

# Call the request method on the Adapter
adapter.request()
