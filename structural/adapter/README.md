# Exercise: Adapter Design Pattern

In this exercise, you will practice implementing the Adapter design pattern.

## Instructions

1. Open the `adapter.py` file.

2. Implement the Adapter pattern to make two incompatible classes work together by creating an adapter class that adapts the interface of one class to the other.

3. Define a `Target` interface or base class that represents the interface expected by the client code.

4. Create an `Adaptee` class that represents the existing class with an incompatible interface.

5. Implement an `Adapter` class that implements the `Target` interface and internally uses an instance of the `Adaptee` class to perform the required operations.

6. Test your implementation by creating instances of the `Target` interface and using them to interact with the adapted `Adaptee` class.

## Getting Started

The starting point for this exercise is an empty `adapter.py` file. You need to implement the Adapter pattern and create the necessary classes from scratch.

## Example Scenario

Suppose you are building a music player application that supports different audio formats. You have a `MediaPlayer` class that expects an interface with the methods `play()`, `pause()`, and `stop()`. However, you also have an existing `LegacyAudioPlayer` class that has a different interface with methods like `startPlay()`, `stopPlay()`, and `endPlay()`.

Your task is to create an adapter that adapts the `LegacyAudioPlayer` to the `MediaPlayer` interface so that it can seamlessly integrate with your music player application.

## Testing

To test your implementation, you can create instances of the `MediaPlayer` interface, use them to interact with the adapted `LegacyAudioPlayer`, and verify that the expected actions are performed.

## Conclusion

The Adapter design pattern is a powerful tool for making incompatible interfaces work together. It enables the integration and reuse of existing code without modifying the original classes.

By successfully completing this exercise, you will gain a better understanding of the Adapter pattern and its application in real-world scenarios.

Good luck with the exercise!
