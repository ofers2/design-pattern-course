# solution.py

class Target:
    def request(self):
        pass


class Adaptee:
    def specific_request(self):
        pass


class Adapter(Target):
    def __init__(self, adaptee):
        self.adaptee = adaptee

    def request(self):
        self.adaptee.specific_request()
