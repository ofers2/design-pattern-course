# test_decorator.py

from decorator import ConcreteComponent, EncryptionDecorator, CompressionDecorator

# Create an instance of the ConcreteComponent
component = ConcreteComponent()

# Create a decorator instance with the ConcreteComponent
decorated_component = EncryptionDecorator(component)

# Call the operation method on the decorated component
decorated_component.operation()

# Add another decorator to the decorated component
decorated_component = CompressionDecorator(decorated_component)

# Call the operation method on the newly decorated component
decorated_component.operation()
