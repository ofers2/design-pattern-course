# Exercise: Decorator Design Pattern

In this exercise, you will practice implementing the Decorator design pattern.

## Instructions

1. Open the `decorator.py` file.

2. Implement the Decorator pattern to dynamically add behavior to an object without affecting other instances of the same class.

3. Define a `Component` class or interface that represents the common interface for both the original object and the decorators.

4. Create a `ConcreteComponent` class that implements the `Component` interface and represents the original object to which decorators will be added.

5. Define a `Decorator` class that implements the `Component` interface and has a reference to a `Component` object. The decorator should add additional behavior to the component.

6. Implement one or more concrete decorators by extending the `Decorator` class and adding specific behavior.

7. Test your implementation by creating an instance of the `ConcreteComponent` and adding one or more decorators to it. Verify that the decorators add the expected behavior without affecting other instances of the component.

## Getting Started

The starting point for this exercise is an empty `decorator.py` file. You need to implement the Decorator pattern and create the necessary classes from scratch.

## Example Scenario

Suppose you are building a text processing application that needs to provide additional functionality, such as encryption or compression, to the text data. The Decorator pattern can be used to add these behaviors dynamically without affecting other instances of the text processing component.

In this example, the `Component` represents the common interface for the text processing component and its decorators, while the `ConcreteComponent` represents the original text processing object. The `Decorator` class adds additional behavior to the component, and the concrete decorators (e.g., `EncryptionDecorator`, `CompressionDecorator`) provide specific functionality.

By using the Decorator pattern, you can add or remove decorators as needed to customize the behavior of individual instances of the component, without affecting other instances.

## Testing

To test your implementation, create an instance of the `ConcreteComponent` and add one or more decorators to it. Invoke methods on the component and decorators to verify that the added behavior is functioning as expected.

## Conclusion

The Decorator design pattern provides a flexible way to extend the behavior of objects at runtime, without modifying their structure. It promotes code reusability and modularity in your software systems.

By successfully completing this exercise, you will gain a better understanding of the Decorator pattern and its application in real-world scenarios.

Good luck with the exercise!
