# solution.py

class Component:
    def operation(self):
        pass


class ConcreteComponent(Component):
    def operation(self):
        print("ConcreteComponent operation called")


class Decorator(Component):
    def __init__(self, component):
        self.component = component

    def operation(self):
        self.component.operation()


class EncryptionDecorator(Decorator):
    def operation(self):
        super().operation()
        print("EncryptionDecorator operation called")


class CompressionDecorator(Decorator):
    def operation(self):
        super().operation()
        print("CompressionDecorator operation called")
