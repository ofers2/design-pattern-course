# decorator.py

class Component:
    def operation(self):
        pass


class ConcreteComponent(Component):
    def operation(self):
        pass


class Decorator(Component):
    def __init__(self, component):
        self.component = component

    def operation(self):
        pass


class EncryptionDecorator(Decorator):
    def operation(self):
        pass


class CompressionDecorator(Decorator):
    def operation(self):
        pass
