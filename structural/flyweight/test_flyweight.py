# test_flyweight.py

from flyweight import CharacterFlyweight, Character

# Create shared flyweight objects
flyweight_A = CharacterFlyweight("Arial", 12, "Black")
flyweight_B = CharacterFlyweight("Times New Roman", 14, "Red")

# Create characters with shared flyweights
character_1 = Character("A", flyweight_A)
character_2 = Character("B", flyweight_A)
character_3 = Character("C", flyweight_B)

# Render the characters
character_1.render()
character_2.render()
character_3.render()
