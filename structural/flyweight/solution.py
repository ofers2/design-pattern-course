# solution.py

class CharacterFlyweight:
    def __init__(self, font, size, color):
        self.font = font
        self.size = size
        self.color = color


class Character:
    def __init__(self, char, flyweight):
        self.char = char
        self.flyweight = flyweight

    def render(self):
        print(f"Character '{self.char}': Font={self.flyweight.font}, Size={self.flyweight.size}, Color={self.flyweight.color}")


# Create shared flyweight objects
flyweight_A = CharacterFlyweight("Arial", 12, "Black")
flyweight_B = CharacterFlyweight("Times New Roman", 14, "Red")

# Create characters with shared flyweights
character_1 = Character("A", flyweight_A)
character_2 = Character("B", flyweight_A)
character_3 = Character("C", flyweight_B)

# Render the characters
character_1.render()
character_2.render()
character_3.render()
