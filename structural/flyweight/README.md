# Exercise: Flyweight Design Pattern

In this exercise, you will practice implementing the Flyweight design pattern.

## Instructions

1. Open the `flyweight.py` file.

2. Implement the Flyweight pattern to optimize the memory usage of objects with shared intrinsic states.

3. Identify a class or set of classes that have significant memory usage due to their intrinsic states.

4. Create a `Flyweight` class that represents the shared intrinsic state and can be shared among multiple objects.

5. Modify the original classes to store a reference to the shared `Flyweight` object instead of duplicating the intrinsic state.

6. Test your implementation by creating multiple instances of the modified classes and verifying that they share the same intrinsic state.

## Getting Started

The starting point for this exercise is an empty `flyweight.py` file. You need to implement the Flyweight pattern and modify the necessary classes from scratch.

## Example Scenario

Suppose you are developing a text editor application that allows users to create and manipulate a large number of characters. Each character has intrinsic properties like font, size, and color, which consume significant memory when duplicated across multiple characters.

In this scenario, you can use the Flyweight pattern to optimize the memory usage. You can create a `CharacterFlyweight` class that represents the shared intrinsic properties of a character, such as font and color. The `Character` class can then store a reference to the shared `CharacterFlyweight` object instead of duplicating the intrinsic properties.

By using the Flyweight pattern, you can reduce memory consumption and improve performance by sharing common intrinsic states among multiple objects.

## Testing

To test your implementation, create multiple instances of the modified classes and verify that they share the same intrinsic state. Modify the intrinsic state of one object and verify that it does not affect other objects.

## Conclusion

The Flyweight design pattern is useful when you have a large number of objects that share common intrinsic states. By applying the Flyweight pattern, you can optimize memory usage by sharing the intrinsic states among multiple objects.

Completing this exercise will help you understand the Flyweight pattern and its benefits in reducing memory overhead in certain scenarios.

Good luck with the exercise!
