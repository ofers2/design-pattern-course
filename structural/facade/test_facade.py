# test_facade.py

from facade import MediaPlayerFacade

# Create an instance of the MediaPlayerFacade
media_player = MediaPlayerFacade()

# Use the facade methods to interact with the media player subsystems
media_player.play_audio()
media_player.play_video()
media_player.create_playlist("My Playlist")
