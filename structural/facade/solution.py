# solution.py

class AudioPlayer:
    def play_audio(self):
        print("Playing audio...")


class VideoPlayer:
    def play_video(self):
        print("Playing video...")


class PlaylistManager:
    def create_playlist(self, name):
        print(f"Creating playlist: {name}")


class MediaPlayerFacade:
    def __init__(self):
        self.audio_player = AudioPlayer()
        self.video_player = VideoPlayer()
        self.playlist_manager = PlaylistManager()

    def play_audio(self):
        self.audio_player.play_audio()

    def play_video(self):
        self.video_player.play_video()

    def create_playlist(self, name):
        self.playlist_manager.create_playlist(name)
