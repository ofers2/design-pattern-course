# Exercise: Facade Design Pattern

In this exercise, you will practice implementing the Facade design pattern.

## Instructions

1. Open the `facade.py` file.

2. Implement the Facade pattern to provide a simplified interface to a complex subsystem or set of classes.

3. Identify a complex subsystem or set of classes that you want to encapsulate behind a Facade.

4. Create a `Facade` class that serves as a simplified interface to the subsystem. The facade should delegate the calls to the appropriate classes within the subsystem.

5. Test your implementation by creating an instance of the `Facade` and invoking its methods. Verify that the facade provides a simplified interface to the underlying complex subsystem.

## Getting Started

The starting point for this exercise is an empty `facade.py` file. You need to implement the Facade pattern and create the necessary classes from scratch.

## Example Scenario

Suppose you are developing a media player application that consists of multiple subsystems, including audio player, video player, and playlist manager. Each subsystem has its own set of classes and complex operations.

In this scenario, you can use the Facade pattern to create a `MediaPlayerFacade` that encapsulates the complexity of interacting with the subsystems. The `MediaPlayerFacade` provides high-level methods to play audio, play video, and manage playlists. Internally, the facade delegates the calls to the appropriate classes in the subsystems.

By using the Facade pattern, you can simplify the usage of the media player application for clients, providing a single entry point that hides the complexity of the underlying subsystems.

## Testing

To test your implementation, create an instance of the `MediaPlayerFacade` and use its methods to interact with the media player subsystems. Verify that the facade correctly delegates the calls to the appropriate classes and provides a simplified interface.

## Conclusion

The Facade design pattern promotes simplicity and encapsulation by providing a unified interface to a complex subsystem. It can be beneficial in scenarios where you want to simplify the usage of a set of classes or subsystems by hiding their complexity behind a single entry point.

By successfully completing this exercise, you will gain a better understanding of the Facade pattern and its application in real-world scenarios.

Good luck with the exercise!
