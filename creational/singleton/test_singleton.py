# test_singleton.py

from singleton import Singleton

# Create multiple instances
instance1 = Singleton.get_instance()
instance2 = Singleton.get_instance()

# Verify that both instances refer to the same object
print(instance1 is instance2)  # Output: True
