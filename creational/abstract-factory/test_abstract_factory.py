# test_abstract_factory.py

from abstract_factory import ConcreteFactory1, ConcreteFactory2

# Create concrete factory 1 and its products
factory1 = ConcreteFactory1()
product_a1 = factory1.create_product_a()
product_b1 = factory1.create_product_b()

# Test product a1
print(product_a1.operation_a())  # Output: ConcreteProductA1 operation_a

# Test product b1
print(product_b1.operation_b())  # Output: ConcreteProductB1 operation_b

# Create concrete factory 2 and its products
factory2 = ConcreteFactory2()
product_a2 = factory2.create_product_a()
product_b2 = factory2.create_product_b()

# Test product a2
print(product_a2.operation_a())  # Output: ConcreteProductA2 operation_a

# Test product b2
print(product_b2.operation_b())  # Output: ConcreteProductB2 operation_b
