# solution.py

from abc import ABC, abstractmethod


class Product(ABC):
    @abstractmethod
    def operation(self):
        pass


class ConcreteProductA(Product):
    def operation(self):
        return "ConcreteProductA operation"


class ConcreteProductB(Product):
    def operation(self):
        return "ConcreteProductB operation"


class Creator(ABC):
    @abstractmethod
    def create_product(self) -> Product:
        pass


class ConcreteCreatorA(Creator):
    def create_product(self) -> Product:
        return ConcreteProductA()


class ConcreteCreatorB(Creator):
    def create_product(self) -> Product:
        return ConcreteProductB()
