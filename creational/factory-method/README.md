# Factory Method Design Pattern Exercise

This exercise focuses on implementing the Factory Method design pattern.

## Exercise Instructions

1. Open the `factory_method.py` file.

2. Implement a Factory Method pattern that provides an interface for creating objects but allows subclasses to decide which class to instantiate.

3. Define a Creator class that declares the factory method. This method should return an instance of a Product class.

4. Implement a Product class that represents the objects created by the factory method.

5. Create concrete Creator subclasses that override the factory method and return different types of Product objects.

6. Test the Factory Method implementation by creating instances of the Product objects using the concrete Creators.

## Starting Point

The starting point for this exercise is an empty `factory_method.py` file. You need to implement the Factory Method pattern and create the necessary classes from scratch.

## Solution

A possible solution for this exercise can be found in the `solution.py` file. However, try to solve the exercise on your own before checking the solution.

## Testing

To test your implementation, you can run the provided `test_factory_method.py` script. It contains some test cases to verify the behavior of your Factory Method implementation.

## Conclusion

The Factory Method design pattern is useful when you want to delegate the responsibility of object instantiation to subclasses. It allows for flexibility in creating different types of objects while adhering to a common interface.

Feel free to explore different variations of the Factory Method pattern and experiment with different types of products and creators.

Good luck with the exercise!
