# test_factory_method.py

from factory_method import ConcreteCreatorA, ConcreteCreatorB

# Create concrete creator A and its product
creator_a = ConcreteCreatorA()
product_a = creator_a.create_product()
print(product_a.operation())  # Output: ConcreteProductA operation

# Create concrete creator B and its product
creator_b = ConcreteCreatorB()
product_b = creator_b.create_product()
print(product_b.operation())  # Output: ConcreteProductB operation
