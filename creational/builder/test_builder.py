# test_builder.py

from builder import ConcreteBuilder1, ConcreteBuilder2, Director

# Create a director
director = Director()

# Create builder 1 and construct the product
builder1 = ConcreteBuilder1()
director.set_builder(builder1)
director.construct_product()
product1 = director.get_product()
print(product1.list_parts())  # Output: Part A1, Part B1, Part C1

# Create builder 2 and construct the product
builder2 = ConcreteBuilder2()
director.set_builder(builder2)
director.construct_product()
product2 = director.get_product()
print(product2.list_parts())  # Output: Part A2, Part B2, Part C2
