# test_prototype.py

from prototype import ConcretePrototype1, ConcretePrototype2

# Create prototype 1 and clone it
prototype1 = ConcretePrototype1()
clone1 = prototype1.clone()
print(clone1)  # Output: <__main__.ConcretePrototype1 object at ...>

# Create prototype 2 and clone it
prototype2 = ConcretePrototype2()
clone2 = prototype2.clone()
print(clone2)  # Output: <__main__.ConcretePrototype2 object at ...>
