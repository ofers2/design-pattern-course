# solution.py

from copy import deepcopy


class Prototype:
    def clone(self):
        pass


class ConcretePrototype1(Prototype):
    def clone(self):
        return deepcopy(self)


class ConcretePrototype2(Prototype):
    def clone(self):
        return deepcopy(self)
